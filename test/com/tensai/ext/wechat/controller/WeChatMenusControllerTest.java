package com.tensai.ext.wechat.controller;

import com.alibaba.fastjson.JSON;
import com.tensai.FirstSpringBootApplication;
import com.tensai.ext.wechat.entity.WeChatConfig;
import com.tensai.ext.wechat.service.IWeChatConfigService;
import com.tensai.ext.wechat.service.IWeChatMenusService;
import com.tensai.ext.wechat.utils.AccessToken;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@ContextConfiguration(classes = {FirstSpringBootApplication.class})
public class WeChatMenusControllerTest {

    @Autowired
    private IWeChatMenusService weChatMenusService;

    @Autowired
    private IWeChatConfigService weChatConfigService;

    @Autowired
    private RestTemplate restTemplate;


    @Test
    public void getMenus() {
        WeChatConfig weChatConfig = weChatConfigService.findByCode("chengde");
        Map result = weChatMenusService.getMenus(weChatConfig);
        System.out.println(JSON.toJSON(result));
    }

    @Test
    public void test(){
        String key = "58cd";
        String token = DigestUtils.md5Hex(DigestUtils.md5Hex("chengde" + key) + key);

        String url = "http://demo.58cd.cn/weChat/getAccessToken?code=chengde&token=" + token;

        ResponseEntity<AccessToken> rest = restTemplate.getForEntity(url, AccessToken.class);
        log.info(JSON.toJSONString(rest));

    }
}