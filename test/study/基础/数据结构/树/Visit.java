package study.基础.数据结构.树;

/**
 * 对结点进行操作的接口,规定树的遍历的类必须实现这个接口
 * @author Administrator
 *
 */
public interface Visit {
    /**
     * 对结点进行某种操作
     * @param tree 树的结点
     */
    public void visit(Tree tree);
}

