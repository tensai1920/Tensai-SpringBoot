package study.基础.算法.递归;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Scanner;
import java.util.Stack;

/**
 * @author Tensai
 * 2019/3/29
 */
public class 汉诺塔 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int h = scanner.nextInt();
        Pan a = new Pan(makeStack(h), "A");
        Pan b = new Pan(new Stack<>(), "B");
        Pan c = new Pan(new Stack<>(), "C");
        move(h, a, c, b);
        System.out.println();
    }

    public static Stack<Integer> makeStack(int h) {
        Stack<Integer> stack = new Stack<>();
        for (int i = h; i > 0; i--) {
            stack.push(i);
        }
        return stack;
    }

    private static void move(int h, Pan source, Pan target, Pan temp) {
        if (h <= 1) {
            System.out.println(source.getStack().peek() + "\t" + source.getName() + " --> " + target.getName() + "\t");
            target.getStack().push(source.getStack().pop());
        } else {
            move(h - 1, source, temp, target);
            System.out.println(source.getStack().peek() + "\t" + source.getName() + " --> " + target.getName() + "\t");
            target.getStack().push(source.getStack().pop());
            move(h - 1, temp, target, source);
        }
    }

}

class Pan {
    private Stack<Integer> stack;
    private String name;

    Pan(Stack<Integer> stack, String name) {
        this.stack = stack;
        this.name = name;
    }

    Stack<Integer> getStack() {
        return stack;
    }

    void setStack(Stack<Integer> stack) {
        this.stack = stack;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }
}