package study.问题.解决的问题.驼峰和下划线的JSON互转;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import org.junit.Assert;

/**
 * @author Tensai
 * 2019/3/21
 */
public class Test {

    public static void main(String[] args) {
        // 生产环境中，config要做singleton处理，要不然会存在性能问题
        User user = new User();
        user.setNameInfo("coder");
        user.setAgeInfo("28");
        SerializeConfig config = new SerializeConfig();
        String json = JSON.toJSONString(user, config);
        System.out.println(json);
        Assert.assertEquals("{\"age_info\":\"28\",\"name_info\":\"coder\"}", json);
    }

}

