package study.问题.解决的问题.驼峰和下划线的JSON互转;

/**
 * @author Tensai
 * 2019/3/21
 */
public class User {

    private String nameInfo;

    private String ageInfo;

    public String getNameInfo() {
        return nameInfo;
    }

    public void setNameInfo(String nameInfo) {
        this.nameInfo = nameInfo;
    }

    public String getAgeInfo() {
        return ageInfo;
    }

    public void setAgeInfo(String ageInfo) {
        this.ageInfo = ageInfo;
    }

    public static void main(String[] args) {
        int[] a = new int[4];
        a[0] = 3;
        a[3] = 5;
        for (int s : a) {
            System.out.println(s);
        }
    }
}

