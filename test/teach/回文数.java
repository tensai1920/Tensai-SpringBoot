package teach;

import org.w3c.dom.stylesheets.LinkStyle;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 回文数知识点
 *
 * @author Tensai
 * 2019/3/10
 */
public class 回文数 {

    public static void main(String[] args) {
        do {
            System.out.println("请输入一个数字，让计算机判断是不是回文数，输入数字为0，则会退出程序");
            Scanner scanner = new Scanner(System.in);
            int flag = scanner.nextInt();
            if (flag == 0) {
                return;
            }
            method2(flag);
        } while (true);

    }

    /**
     * 第一种方法判断回文数
     *
     * @param t 输入的参数
     */
    private static void method1(int t) {
        System.out.println("参数t为：\t" + t);
        int shang = t;
        List<Integer> temp = new ArrayList<>();
        while (shang != 0) {
            int yushu = shang % 10;
            System.out.println(yushu);
            temp.add(yushu);
            shang = shang / 10;
        }
        int res = 0;
        for (int e : temp) {
            res = res * 10 + e;
        }
        System.out.println("参数t倒转为：\t" + res);
    }

    private static void method2(int s) {
        System.out.println("参数s为：\t" + s);
        int temp = s;
        int n = 0;
        while (temp != 0) {
            temp = temp / 10;
            n++;
        }
        System.out.println("size = " + n);
        temp = s;
        int[] yushus = new int[n];
        for (int i = 0; i < n; i++) {
            int yushu = temp % 10;
            System.out.println(yushu);
            yushus[i] = yushu;
            temp = temp / 10;
        }
        for (int i = 0; i < n; i++) {
            System.out.println("余数数组第 " + i + " 个元素为： "+ yushus[i]);
        }

        int result = 0;
        for (int i = 0; i < n; i ++) {
            int shuzi;
            int chengfang = 1;
            for (int j = 0 ; j < n - i -1; j++) {
                chengfang = chengfang*10;
            }
            shuzi = yushus[i] * chengfang;
            result = result + shuzi;
        }
        System.out.println("参数s倒转为：\t" + result);
        if (s == result) {
            System.out.println("判断为回文数");
        } else {
            System.out.println("判断不是不回文数");
        }
    }
}


