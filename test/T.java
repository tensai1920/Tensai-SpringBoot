import com.alibaba.fastjson.JSON;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tensai
 * 2019/1/7
 */
public class T {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        System.out.println(JSON.toJSONString(list));
        String menus = "3,5,2,4,";
        String[] checked = menus.split(",");
        System.out.println(JSON.toJSONString(checked));
    }

}

