package com.tensai.ext.wechat.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tensai.ext.wechat.entity.WeChatConfig;
import com.tensai.ext.wechat.service.IWeChatQRCodeService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author kissy
 */
@Slf4j
@Service
public class WeChatQRCodeServiceImpl extends WeChatBase implements IWeChatQRCodeService {

    @Override
    public String getTempQRCode(int expireSeconds, String sceneStr, WeChatConfig weChatConfig) {
        String tmpurl = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=" + getWXToken(weChatConfig).getAccessToken();
        JSONObject json = new JSONObject();
        json.put("expire_seconds", expireSeconds);
        json.put("action_name", "QR_STR_SCENE");
        JSONObject scene = new JSONObject();
        JSONObject action = new JSONObject();
        scene.put("scene_str", sceneStr);
        action.put("scene", scene);
        json.put("action_info", action);
        try {
            //TODO
//            JSONObject result = httpsRequest(tmpurl, "POST", json.toString());
            JSONObject result = new JSONObject();
            JSONObject resultJson = new JSONObject(result);
            log.info("发送微信消息返回信息：" + resultJson.toJSONString());
            if (result.get("url") == null) {
                return null;
            } else {
                return result.get("url").toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
