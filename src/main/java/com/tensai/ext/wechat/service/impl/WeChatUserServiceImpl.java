package com.tensai.ext.wechat.service.impl;

import com.tensai.ext.wechat.entity.WeChatConfig;
import com.tensai.ext.wechat.service.IWeChatUserService;
import org.springframework.stereotype.Service;

import java.util.Map;


/**
 * @author kissy
 */
@Service
public class WeChatUserServiceImpl extends WeChatBase implements IWeChatUserService {

    @Override
    public Map getTags(WeChatConfig weChatConfig) {
        String url = "https://api.weixin.qq.com/cgi-bin/tags/get?access_token=" + getWXToken(weChatConfig).getAccessToken();
        return getRestTemplate().getForEntity(url, Map.class).getBody();
    }

    @Override
    public Map findByOpenId(String openId, WeChatConfig weChatConfig) {
        String url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=" + getWXToken(weChatConfig).getAccessToken() + "&openid=" + openId + "&lang=zh_CN";
        return getRestTemplate().getForEntity(url, Map.class).getBody();
    }
}
