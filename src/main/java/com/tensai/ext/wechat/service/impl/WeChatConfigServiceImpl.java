package com.tensai.ext.wechat.service.impl;


import com.tensai.core.mvc.base.service.imp.BaseIdServiceImpl;
import com.tensai.ext.wechat.dao.IWeChatConfigDao;
import com.tensai.ext.wechat.entity.WeChatConfig;
import com.tensai.ext.wechat.service.IWeChatConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * 微信配置业务
 *
 * @author Tensai
 * 2018/12/20
 */
@Service
public class WeChatConfigServiceImpl extends BaseIdServiceImpl<WeChatConfig, IWeChatConfigDao> implements IWeChatConfigService {

    @Autowired
    private IWeChatConfigDao weChatConfigDao;

    @Override
    protected IWeChatConfigDao getDao() {
        return weChatConfigDao;
    }

    @Cacheable(key = "'wechat:config:'+#code", value = "no_limit")
    @Override
    public WeChatConfig findByCode(String code) {
        WeChatConfig weChatConfig = getDao().findByCode(code);
        if (weChatConfig == null) {
            return getDao().findOne(1);
        }
        return weChatConfig;
    }
}

