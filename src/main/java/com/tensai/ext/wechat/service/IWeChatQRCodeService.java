package com.tensai.ext.wechat.service;


import com.tensai.ext.wechat.entity.WeChatConfig;

/**
 * @author kissy
 */
public interface IWeChatQRCodeService {

    /**
     * 获取临时场景二维码
     *
     * @param expireSeconds 过期时间
     * @param sceneStr      场景参数
     * @param weChatConfig  微信配置
     * @return 场景二维码URL
     */
    String getTempQRCode(int expireSeconds, String sceneStr, WeChatConfig weChatConfig);
}
