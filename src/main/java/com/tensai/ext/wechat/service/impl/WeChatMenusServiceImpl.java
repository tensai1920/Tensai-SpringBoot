package com.tensai.ext.wechat.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tensai.ext.wechat.entity.WeChatConfig;
import com.tensai.ext.wechat.service.IWeChatMenusService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author Tensai
 */
@Service
public class WeChatMenusServiceImpl extends WeChatBase implements IWeChatMenusService {

    @Override
    public Map createMenus(JSONObject menus, WeChatConfig weChatConfig) {
        String url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=" + getWXToken(weChatConfig).getAccessToken();
        return getRestTemplate().postForEntity(url, menus, Map.class).getBody();
    }

    @Override
    public Map getMenus(WeChatConfig weChatConfig) {
        String url = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=" + getWXToken(weChatConfig).getAccessToken();
        return getRestTemplate().getForEntity(url, Map.class).getBody();
    }

    @Override
    public Map deleteAll(WeChatConfig weChatConfig) {
        String url = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=" + getWXToken(weChatConfig).getAccessToken();
        return getRestTemplate().getForEntity(url, Map.class).getBody();
    }

    @Override
    public Map addMenus(JSONObject menus, WeChatConfig weChatConfig) {
        String url = "https://api.weixin.qq.com/cgi-bin/menu/addconditional?access_token=" + getWXToken(weChatConfig).getAccessToken();
        return getRestTemplate().postForEntity(url, menus, Map.class).getBody();
    }

    @Override
    public Map deleteMenus(JSONObject menuId,WeChatConfig weChatConfig) {
        String url = "https://api.weixin.qq.com/cgi-bin/menu/delconditional?access_token=" + getWXToken(weChatConfig).getAccessToken();
        return getRestTemplate().postForEntity(url, menuId, Map.class).getBody();
    }

    @Override
    public Map tryMatchMenus(JSONObject userId,WeChatConfig weChatConfig) {
        String url = "https://api.weixin.qq.com/cgi-bin/menu/trymatch?access_token=" + getWXToken(weChatConfig).getAccessToken();
        return getRestTemplate().postForEntity(url, userId, Map.class).getBody();
    }
}
