package com.tensai.ext.wechat.service;


import com.tensai.ext.wechat.entity.WeChatConfig;

import java.util.Map;

/**
 * @author kissy
 */
public interface IWeChatUserService {

    /**
     * 根据标签获取用户
     * @param weChatConfig  微信公众号配置
     * @return  用户组
     */
    Map getTags(WeChatConfig weChatConfig);

    /**
     * 根据openid获取用户
     * @param openId        openid
     * @param weChatConfig  微信公众号配置
     * @return  用户信息
     */
    Map findByOpenId(String openId, WeChatConfig weChatConfig);
}
