package com.tensai.ext.wechat.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.tensai.ext.wechat.entity.WeChatConfig;
import com.tensai.ext.wechat.service.IWeChatMsgService;
import com.tensai.ext.wechat.utils.TemplateData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author lxc
 * @date 2018/4/9
 */
@Service
public class WeChatMsgServiceImpl extends WeChatBase implements IWeChatMsgService {


    /**
     * 获取所有用户的OpenID
     *
     * @return
     */
    @Override
    public JSONArray getOpenId(WeChatConfig weChatConfig) {
        JSONArray jsonArray = new JSONArray();
        String token = getWXToken(weChatConfig).getAccessToken();
        String tokenUrl = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=" + token;
        //TODO
//        JSONObject jsonObject = httpsRequest(tokenUrl, "GET", null);
//        if (null != jsonObject) {
//            try {
//                jsonObject = JSONObject.parseObject(jsonObject.getString("data"));
//                jsonArray = JSONObject.parseArray(jsonObject.getString("openid"));
//            } catch (JSONException e) {
//                // 获取token失败
//                log.error("获取openId失败 errcode:{} errmsg:{}", jsonObject.getInteger("errcode"), jsonObject.getString("errmsg"));
//            }
//        }
        return jsonArray;
    }

    @Override
    public String getCode(WeChatConfig weChatConfig) {
        return null;
    }

    /**
     * 封装模板详细信息
     *
     * @return JsonObject对象
     */
    private static JSONObject packJsonmsg(Map<String, TemplateData> param) {
        JSONObject json = new JSONObject();
        for (Map.Entry<String, TemplateData> entry : param.entrySet()) {
            JSONObject keyJson = new JSONObject();
            TemplateData dta = entry.getValue();
            keyJson.put("value", dta.getValue());
            keyJson.put("color", dta.getColor());
            json.put(entry.getKey(), keyJson);
        }
        return json;
    }

    /**
     * 通过code找到Openid
     *
     * @param code
     * @return
     */
    @Override
    public String getOpenIdByCode(String code, WeChatConfig weChatConfig) {
        String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + weChatConfig.getAppId() + "&secret=" + weChatConfig.getAppSecret() + "&code=" + code + "&grant_type=authorization_code";
        String openId = "";
        //TODO
//        try {
//            JSONObject jsonObject = httpsRequest(url, "GET", null);
//            openId = jsonObject.getString("openid");
//        } catch (Exception e) {
//            log.error("根据CODE获取openID失败");
//        }
        return openId;
    }

    /**
     * 发送微信消息(模板消息)
     *
     * @param touser    用户 OpenID
     * @param templatId 模板消息ID
     * @param clickurl  URL置空，则在发送后，点击模板消息会进入一个空白页面（ios），或无法点击（android）。
     * @param topcolor  标题颜色
     * @param data      详细内容
     * @return 发送状态 The status of send
     */
    @Override
    public String sendWechatMsgToUser(String touser, String templatId, String clickurl, String topcolor, JSONObject data, WeChatConfig weChatConfig) {
        String tmpurl = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + getWXToken(weChatConfig).getAccessToken();
        JSONObject json = new JSONObject();
        json.put("touser", touser);
        json.put("template_id", templatId);
        json.put("url", clickurl);
        json.put("topcolor", topcolor);
        json.put("data", data);
        //TODO
//        try {
//            JSONObject result = httpsRequest(tmpurl, "POST", json.toString());
//            JSONObject resultJson = new JSONObject(result);
//            log.info("发送微信消息返回信息：" + resultJson.get("errcode"));
//            String errmsg = (String) resultJson.get("errmsg");
//            //如果为errmsg为ok，则代表发送成功，公众号推送信息给用户了。
//            if (!"ok".equals(errmsg)) {
//                return "error";
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            return "error";
//        }
        return "success";
    }

    /**
     * 推送告警信息
     *
     * @param openId 用户id
     * @param frist  标题
     * @param key1   设备名称
     * @param key2   告警名称
     * @param key3   告警时间
     * @param key4   告警数值
     * @param remark 详细信息
     * @param url    点击链接
     * @return
     */
    @Override
    public String sendMsg(String openId, String frist, String key1, String key2, String key3, String key4, String remark, String url, WeChatConfig weChatConfig) {
        Map<String, TemplateData> param = new HashMap<>();
        param.put("first", new TemplateData(frist, "#458B00"));
        param.put("keyword1", new TemplateData(key1, "#4876FF"));
        param.put("keyword2", new TemplateData(key2, "#FF3030"));
        param.put("keyword3", new TemplateData(key3, "#A0522D"));
        param.put("keyword4", new TemplateData(key4, "#D2691E"));
        param.put("remark", new TemplateData(remark, "#FF00FF"));
        //注册的微信-模板Id
        String regTempId = weChatConfig.getTempId();
        //调用发送微信消息给用户的接口
        return sendWechatMsgToUser(openId, regTempId, url, "#FF0000", packJsonmsg(param), weChatConfig);
    }
}
