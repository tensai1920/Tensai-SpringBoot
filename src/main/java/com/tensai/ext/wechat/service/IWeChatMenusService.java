package com.tensai.ext.wechat.service;

import com.alibaba.fastjson.JSONObject;
import com.tensai.ext.wechat.entity.WeChatConfig;

import java.util.Map;

/**
 * @author kissy
 */
public interface IWeChatMenusService {

    //以下是自定义菜单接口
    /**
     * https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN
     *
     * @param menus        菜单
     * @param weChatConfig 微信公众号配置
     * @return 创建菜单结果
     */
    Map createMenus(JSONObject menus, WeChatConfig weChatConfig);

    /**
     * https://api.weixin.qq.com/cgi-bin/menu/get?access_token=ACCESS_TOKEN
     *
     * @param weChatConfig 微信公众号配置
     * @return 获取自定义菜单
     */
    Map getMenus(WeChatConfig weChatConfig);

    /**
     * https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN
     *
     * @param weChatConfig 微信公众号配置
     * @return 删除菜单执行结果
     */
    Map deleteAll(WeChatConfig weChatConfig);


    //以下是个性化菜单的接口
    /**
     * 添加接口（主要）
     * https://api.weixin.qq.com/cgi-bin/menu/addconditional?access_token=ACCESS_TOKEN
     *
     * @param weChatConfig 微信公众号配置
     * @param menus        菜单
     * @return 添加菜单执行结果
     */
    Map addMenus(JSONObject menus, WeChatConfig weChatConfig);

    /**
     * 删除个性化菜单
     *
     * @param menuId       菜单id
     * @param weChatConfig 微信配置
     * @return 执行结果
     */
    Map deleteMenus(JSONObject menuId, WeChatConfig weChatConfig);

    /**
     * 测试菜单匹配
     *
     * @param userId       用户id
     * @param weChatConfig 微信配置
     * @return 返回结果
     */
    Map tryMatchMenus(JSONObject userId, WeChatConfig weChatConfig);


}
