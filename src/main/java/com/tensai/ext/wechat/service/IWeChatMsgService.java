package com.tensai.ext.wechat.service;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tensai.ext.wechat.entity.WeChatConfig;

/**
 * 微信告警推送
 *
 * @author lxc
 * 2018/4/10
 */
public interface IWeChatMsgService {

    /**
     * 获取所有用户的OpenID
     *
     * @param weChatConfig 微信公众号配置
     * @return 所有用户
     */
    JSONArray getOpenId(WeChatConfig weChatConfig);

    /**
     * 获取微信用户的code
     *
     * @param weChatConfig 微信公众号配置
     * @return 用户code
     */
    String getCode(WeChatConfig weChatConfig);

    /**
     * 根据Code获取用户的OpenID
     *
     * @param code         用户code
     * @param weChatConfig 微信公众号配置
     * @return 用户OpenId
     */
    String getOpenIdByCode(String code, WeChatConfig weChatConfig);

    /**
     * 向用户推送告警消息
     *
     * @param touser       用户
     * @param templatId    模板
     * @param clickurl     点击的url
     * @param topcolor     颜色
     * @param data         数据
     * @param weChatConfig 微信公众号配置
     * @return 推送告警结果
     */
    String sendWechatMsgToUser(String touser, String templatId, String clickurl, String topcolor, JSONObject data, WeChatConfig weChatConfig);

    /**
     * 推送告警信息
     *
     * @param openId 用户id
     * @param first  标题
     * @param key1   设备名称
     * @param key2   告警名称
     * @param key3   告警时间
     * @param key4   告警数值
     * @param remark 详细信息
     * @param url    点击链接
     * @return 推送告警结果
     */
    String sendMsg(String openId, String first, String key1, String key2, String key3, String key4, String remark, String url, WeChatConfig weChatConfig);


}
