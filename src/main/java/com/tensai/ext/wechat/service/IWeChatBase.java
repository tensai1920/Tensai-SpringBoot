package com.tensai.ext.wechat.service;

import com.tensai.ext.wechat.entity.WeChatConfig;
import com.tensai.ext.wechat.utils.AccessToken;

/**
 * 微信基础Service
 * @author kissy
 */
public interface IWeChatBase {

    /**
     * 获取AccessToken
     * @param weChatConfig 微信公众号配置
     * @return  accessToken
     */
    AccessToken getWXToken(WeChatConfig weChatConfig);

}
