package com.tensai.ext.wechat.service;

import com.tensai.core.mvc.base.service.IIdService;
import com.tensai.ext.wechat.dao.IWeChatConfigDao;
import com.tensai.ext.wechat.entity.WeChatConfig;

/**
 * 微信公众号配置
 * @author kissy
 */
public interface IWeChatConfigService extends IIdService<WeChatConfig, IWeChatConfigDao> {

    /**
     * 通过微信代码获取配置
     * @param code  微信代码
     * @return  微信配置
     */
    WeChatConfig findByCode(String code);
}
