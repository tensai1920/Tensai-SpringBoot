package com.tensai.ext.wechat.controller;

import com.tensai.ext.wechat.entity.WeChatConfig;
import com.tensai.ext.wechat.service.IWeChatConfigService;
import com.tensai.ext.wechat.service.IWeChatUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/weChat/user")
public class WeChatUserController{

    @Autowired
    private IWeChatUserService weChatUserService;


    @Autowired
    private IWeChatConfigService weChatConfigService;


    @RequestMapping(value = "getTags")
    @ResponseBody
    public HashMap<String, Object> getTags(String code) {
        WeChatConfig weChatConfig = weChatConfigService.findByCode(code);
        Map map = weChatUserService.getTags(weChatConfig);
        return (HashMap<String, Object>) map;
    }

    @RequestMapping(value = "findByOpenId")
    @ResponseBody
    @Cacheable(value = "1_month", key = "'findByOpenId:' + #openId + ':' + #code")
    public HashMap<String, Object> findByOpenId(String openId, String code) {
        WeChatConfig weChatConfig = weChatConfigService.findByCode(code);
        Map map = weChatUserService.findByOpenId(openId, weChatConfig);
        return (HashMap<String, Object>) map;
    }

}
