package com.tensai.ext.wechat.controller;

import com.tensai.ext.wechat.entity.WeChatConfig;
import com.tensai.ext.wechat.service.IWeChatBase;
import com.tensai.ext.wechat.service.IWeChatConfigService;
import com.tensai.ext.wechat.service.IWeChatQRCodeService;
import com.tensai.ext.wechat.utils.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author kissy
 */
@Controller
@RequestMapping("/ext/weChat/code/")
public class WeChatCodeController {

    @Autowired
    private IWeChatQRCodeService weChatQRCodeService;

    @Autowired
    private IWeChatConfigService weChatConfigService;

    @Autowired
    private IWeChatBase weChatBase;

    /**
     * 获取微信临时二维码
     *
     * @param expireSeconds 过期时间
     * @param params        二维码参数，格式：机器id,广告id
     * @param code          微信公众号代码
     * @return 二维码的字符串
     */
    @RequestMapping(value = "/getCode")
    @ResponseBody
    public Object getQRCode(int expireSeconds, String params, String code) {
        WeChatConfig weChatConfig = weChatConfigService.findByCode(code);
        return weChatQRCodeService.getTempQRCode(expireSeconds, params, weChatConfig);
    }

    @RequestMapping(value = "/test")
    @ResponseBody
    public AccessToken test() {
        WeChatConfig weChatConfig = weChatConfigService.findByCode(null);
        return weChatBase.getWXToken(weChatConfig);
    }

}
