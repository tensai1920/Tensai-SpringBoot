package com.tensai.ext.wechat.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 微信网页端使用的接口
 *
 * @author Tensai
 * 2019/3/12
 */
@RestController
@RequestMapping("/wechat/web/")
public class WeChatWebController {

}

