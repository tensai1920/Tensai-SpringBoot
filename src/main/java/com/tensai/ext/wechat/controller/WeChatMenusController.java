package com.tensai.ext.wechat.controller;

import com.alibaba.fastjson.JSONObject;
import com.tensai.ext.wechat.entity.WeChatConfig;
import com.tensai.ext.wechat.service.IWeChatConfigService;
import com.tensai.ext.wechat.service.IWeChatMenusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 微信公众号菜单操作
 *
 * @author kissy
 */
@RestController
@RequestMapping("/weChat/menus")
public class WeChatMenusController {

    @Autowired
    private IWeChatMenusService weChatMenusService;

    @Autowired
    private IWeChatConfigService weChatConfigService;

    /**
     * 创建自定义菜单
     *
     * @param menus 菜单
     * @param code  微信公众平台编码
     * @return 返回值
     */
    @RequestMapping(value = "/createMenus")
    public Map createMenus(@RequestBody JSONObject menus, String code) {
        WeChatConfig weChatConfig = weChatConfigService.findByCode(code);
        Map result = weChatMenusService.createMenus(menus, weChatConfig);
        return result;
    }

    /**
     * @param code
     * @return
     */
    @RequestMapping(value = "/getMenus")
    public Map getMenus(String code) {
        WeChatConfig weChatConfig = weChatConfigService.findByCode(code);
        Map result = weChatMenusService.getMenus(weChatConfig);
        return result;
    }


    /**
     * 添加个性化菜单
     *
     * @param menus 菜单
     * @param code  微信公众平台编码
     * @return 返回值
     */
    @RequestMapping(value = "/addMenus")
    public Map addMenus(@RequestBody JSONObject menus, String code) {
        WeChatConfig weChatConfig = weChatConfigService.findByCode(code);
        Map result = weChatMenusService.addMenus(menus, weChatConfig);
        return result;
    }

    public Map tryMatchMenus(JSONObject userId, String code) {
        WeChatConfig weChatConfig = weChatConfigService.findByCode(code);
        Map result = weChatMenusService.tryMatchMenus(userId, weChatConfig);
        return result;
    }


    /**
     * 删除所有菜单
     *
     * @param code 微信公众平台编码
     * @return 操作结果
     */
    @RequestMapping(value = "/deleteAll")
    public Map deleteAll(String code) {
        WeChatConfig weChatConfig = weChatConfigService.findByCode(code);
        Map result = weChatMenusService.deleteAll(weChatConfig);
        return result;
    }

}


