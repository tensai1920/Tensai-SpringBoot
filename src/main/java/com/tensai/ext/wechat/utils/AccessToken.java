package com.tensai.ext.wechat.utils;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * accessToken类
 *
 * @author Tensai
 * 2018/4/9
 */
@Getter
@Setter
public class AccessToken implements Serializable {

    private String accessToken;

    private int expiresIn;

}
