package com.tensai.ext.wechat.dao;


import com.tensai.core.mvc.base.dao.IIdDao;
import com.tensai.ext.wechat.entity.WeChatConfig;

/**
 * 微信配置
 * @author kissy
 */
public interface IWeChatConfigDao extends IIdDao<WeChatConfig> {

    /**
     * 根据
     * @param code 微信公众号代码
     * @return  微信公众号配置
     */
    WeChatConfig findByCode(String code);
}
