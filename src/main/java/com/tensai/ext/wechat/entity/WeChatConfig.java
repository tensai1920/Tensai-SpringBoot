package com.tensai.ext.wechat.entity;


import com.tensai.core.mvc.base.entry.IdEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 微信配置实体
 *
 * @author Tensai
 * 2018/12/18
 */
@Data
@Entity
@Table(name = "ext_wechat_config")
public class WeChatConfig extends IdEntity {

    /**
     * APP_ID
     */
    @Column
    private String appId;
    /**
     * APP_SECRET
     */
    private String appSecret;
    /**
     * 模板ID
     */
    @Column
    private String tempId;
    /**
     * 重定向的host
     */
    @Column
    private String redirect;
    /**
     * 微信公众号code
     */
    @Column
    private String code;
    /**
     * 微信公众号名称
     */
    @Column
    private String name;

}

