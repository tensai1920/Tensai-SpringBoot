package com.tensai.core.utils;

import com.tensai.core.mvc.base.entry.IdEntity;

import java.util.List;

/**
 * 集合工具类
 *
 * @author Tensai
 * 2019/1/11
 */
public class ListUtils {

    public static<T extends IdEntity> void filterList(List<Integer> checked, List<T>... lists) {
        ObjectCheck.checkListAndInit(checked);
        for (List<T> list:lists) {
            ObjectCheck.checkListAndInit(list);
            list.removeIf(item -> !checked.contains(item.getId()));
        }
    }

}

