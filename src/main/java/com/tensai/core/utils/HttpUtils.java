package com.tensai.core.utils;

import org.springframework.http.HttpHeaders;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

/**
 * HTTP请求的工具类
 *
 * @author Tensai
 * 2019/1/21
 */
public class HttpUtils {

    public static HttpHeaders makeHeaders(Map<String,String> headerMap) {
        HttpHeaders httpHeaders = new HttpHeaders();
        if (headerMap != null) {
            headerMap.forEach(httpHeaders::add);
        }
        return httpHeaders;
    }

    public static StringBuffer getParams(Map<String,String> getParams){
        StringBuffer params = new StringBuffer();
        if (getParams != null) {
            String and = null;
            try {
                and = URLEncoder.encode("&", "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String finalAnd = and;
            getParams.forEach((key, value) -> {
                params.append(key).append("=").append(value).append(finalAnd);
            });
        }
        return params;
    }

}

