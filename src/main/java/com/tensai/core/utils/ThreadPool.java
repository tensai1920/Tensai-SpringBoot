package com.tensai.core.utils;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

/**
 *	线程池
 * @author Tensai
 *         2018/1/12
 */
public enum ThreadPool {
	/**
	 * 枚举
	 */
	INSTANCE;
	Map<String, ThreadPoolExecutor> threadPoolMap = new HashMap<>();

	/**
	 * 线程池获取工厂
	 * @param name	线程池名称
	 * @return	线程池
	 */
	public static ThreadPoolExecutor getPool(String name){
		if (ThreadPool.INSTANCE.threadPoolMap == null || ThreadPool.INSTANCE.threadPoolMap.get(name) == null) {
			ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat(name + "-%d").build();
			
			ThreadPoolExecutor pool = new ThreadPoolExecutor(10, 50, 3000,
					TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(1024), threadFactory,
					new ThreadPoolExecutor.AbortPolicy());
			ThreadPool.INSTANCE.threadPoolMap.put(name,pool);
			return pool;
		} else {
			return ThreadPool.INSTANCE.threadPoolMap.get(name);
		}
	}
	
	
	
}
