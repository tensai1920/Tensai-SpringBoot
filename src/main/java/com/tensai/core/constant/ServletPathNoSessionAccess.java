package com.tensai.core.constant;

/**
 * 允许无Session访问的路径
 *
 * @author Tensai
 * 2018-03-09 10:50
 */
public enum ServletPathNoSessionAccess {
    /**
     * 登录、html文件、首页index、测试
     */
    LOGIN("login"), HTML("html"), INDEX("index"), Test("test");

    String value;

    ServletPathNoSessionAccess(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
