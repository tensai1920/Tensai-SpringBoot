package com.tensai.core.constant;

/**
 * HQL拼接的常量定义类
 *
 * @author Tensai
 * 2018-03-09 10:55
 */
public interface HqlMark {

    /**
     * String
     */
    String TYPE_S = "S";
    String TYPE_L = "L";
    String TYPE_I = "I";
    String TYPE_D = "D";
    String TYPE_ST = "ST";
    String TYPE_BD = "BD";
    String TYPE_FT = "FT";

    String OPERATOR_LK = "LK";
    String OPERATOR_RLK = "RLK";
    String OPERATOR_LLK = "LLK";
}
