package com.tensai.core.constant;

/**
 * 未定义的常量
 *
 * @author Tensai
 * 2018-03-09 10:51
 */
public interface UndefinedField {

    Integer ADMIN_UUID = 0;
}
