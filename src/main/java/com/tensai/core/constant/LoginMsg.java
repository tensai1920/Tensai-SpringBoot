package com.tensai.core.constant;

/**
 * 登录的状态信息
 *
 * @author Tensai
 */
public interface LoginMsg {
    String SUCCESS = "登录成功";
    String FAILURE = "登录失败";
}
