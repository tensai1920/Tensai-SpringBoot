package com.tensai.core.constant;

/**
 * HTTP协议的常量类
 *
 * @author Tensai
 * 2018_03_01 16:33
 */
public interface HttpProtocol {

    String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
    String ACCESS_CONTROL_ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials";
    String ACCESS_CONTROL_ALLOW_METHODS = "Access-Control-Allow-Methods";
    String ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";
    String ACCESS_CONTROL_MAX_AGE = "Access-Control-Max-Age";
    String X_REQUESTED_WITH = "X-Requested-With";
    String XML_HTTP_REQUEST = "XML_HTTP_REQUEST";
}
