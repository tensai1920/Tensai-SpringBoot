package com.tensai.core.constant;

/**
 * 请求异常状态码
 *
 * @author Tensai
 * 2018-03-10 14:43
 */
public enum ResponseStatus {
    /**
     * 响应
     */
    EXCEPTION(-20, "产生异常"),
    ERROR_PARAMS(-10, "参数错误"),
    LOGIN_SUCCESS(1, "登录成功"), LOGIN_FAILURE(0, "登录失败"), LOGOUT_SUCCESS(-1, "退出登录"), LOGIN_TIMOUT(-1, "登陆超时"),
    SAVE_FAILURE(10, "保存更新失败"),SAVE_SUCCESS(11, "保存更新成功"),
    DELETE_SUCCESS(21, "删除成功"), DELETES_SUCCESS(22, "批量删除成功"),
    QUERY_SUCCESS(31, "查询成功");

    /**
     * 响应状态码
     */
    int status;
    /**
     * 相应信息
     */
    String msg;

    ResponseStatus(int status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
