package com.tensai.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * 并不知道具体作用和底层原理，还需学习
 *
 * @author Tensai
 * 2018-03-22 15:40
 */
@Configuration
public class RequestMappingCfg {

    @Bean
    public RequestMappingHandlerMapping requestMappingHandlerMapping() {
        return new RequestMappingHandlerMapping();
    }
}
