package com.tensai.core.mvc.common;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * 上传组件控制器
 *
 * @author Tensai
 * 2019/2/26
 */
@RequestMapping("/common/upload/")
@RestController
public class UploadController {

    @Value("${path.saveFile.img}")
    private String imgSavePath;

    @RequestMapping("/uploadImg")
    @ResponseBody
    public void uploadImg(MultipartFile file){
        try {
            upload(file, imgSavePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void upload(MultipartFile file, String path) throws IOException {
        File saveFile = new File(path);
        OutputStream outputStream = new FileOutputStream(saveFile);
        IOUtils.copy(file.getInputStream(), outputStream);
    }

}

