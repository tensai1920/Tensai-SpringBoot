package com.tensai.core.mvc.system.dao;

import com.tensai.core.mvc.base.dao.IIdDao;
import com.tensai.core.mvc.system.entry.SysResource;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Tensai
 *         2018-03-22 16:09
 */
@Repository
public interface ISysResourceDao extends IIdDao<SysResource> {
	
	List<SysResource> findByResourceString(String url);

	List<SysResource> findByLevel(Integer level);

	List<SysResource> findByType(String type);

	List<SysResource> findByTypeAndParent(String type, SysResource sysResource);

}
