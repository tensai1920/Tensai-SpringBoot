package com.tensai.core.mvc.system.entry;

import com.tensai.core.mvc.base.entry.IdEntity;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.util.List;

/**
 * 系统菜单
 *
 * @author Tensai
 * 2018/12/29
 */
@Entity
@Table(name = "sys_menu")
public class SysMenu extends IdEntity {

    private String name;
    private String viewUrl;
    private Integer sort;
    private String type;
    private Integer level;
    private SysMenu parent;
    private String icon;
    private List<SysMenu> sysMenuList;

    @Column
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column
    public String getViewUrl() {
        return viewUrl;
    }

    public void setViewUrl(String viewUrl) {
        this.viewUrl = viewUrl;
    }

    @Column
    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Column
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column
    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @JoinColumn
    @ManyToOne
    public SysMenu getParent() {
        return parent;
    }

    public void setParent(SysMenu parent) {
        this.parent = parent;
    }

    @Column
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Transient
    public List<SysMenu> getSysMenuList() {
        return sysMenuList;
    }

    public void setSysMenuList(List<SysMenu> sysMenuList) {
        this.sysMenuList = sysMenuList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof SysMenu)) {
            return false;
        }

        SysMenu sysMenu = (SysMenu) o;

        return new EqualsBuilder()
                .append(getName(), sysMenu.getName())
                .append(getViewUrl(), sysMenu.getViewUrl())
                .append(getSort(), sysMenu.getSort())
                .append(getType(), sysMenu.getType())
                .append(getLevel(), sysMenu.getLevel())
                .append(getParent(), sysMenu.getParent())
                .append(getIcon(), sysMenu.getIcon())
                .append(getSysMenuList(), sysMenu.getSysMenuList())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getName())
                .append(getViewUrl())
                .append(getSort())
                .append(getType())
                .append(getLevel())
                .append(getParent())
                .append(getIcon())
                .append(getSysMenuList())
                .toHashCode();
    }
}

