package com.tensai.core.mvc.system.service;

import com.tensai.core.mvc.base.service.IIdService;
import com.tensai.core.mvc.base.tools.XTree;
import com.tensai.core.mvc.system.dao.ISysMenuDao;
import com.tensai.core.mvc.system.entry.SysMenu;
import com.tensai.core.mvc.system.facade.UserMenu;

import java.util.List;


public interface ISysMenuService extends IIdService<SysMenu, ISysMenuDao> {

    /**
     * 获取菜单
     * @return  菜单树
     */
    List<UserMenu> getUserMenus();

    /**
     * 获取菜单权限
     * @param checkedList   菜单选中情况
     * @return              菜单权限
     */
    List<XTree> getUserMenus(List<Integer> checkedList);

    List<SysMenu> findByType(String type);
}
