package com.tensai.core.mvc.system.dao;

import com.tensai.core.mvc.base.dao.IIdDao;
import com.tensai.core.mvc.system.entry.SysMenu;

import java.util.List;

public interface ISysMenuDao extends IIdDao<SysMenu> {

    /**
     * 查询某个级别的菜单
     * @param level 菜单级别
     * @return      菜单列表
     */
    List<SysMenu> findByLevel(Integer level);

    List<SysMenu> findByType(String type);
}
