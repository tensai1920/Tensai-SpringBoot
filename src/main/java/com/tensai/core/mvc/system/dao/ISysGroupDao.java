package com.tensai.core.mvc.system.dao;

import com.tensai.core.mvc.base.dao.IIdDao;
import com.tensai.core.mvc.system.entry.SysGroup;

public interface ISysGroupDao extends IIdDao<SysGroup> {

}
