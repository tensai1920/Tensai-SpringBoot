package com.tensai.core.mvc.system.service.imp;

import com.tensai.core.mvc.base.service.imp.BaseIdServiceImpl;
import com.tensai.core.mvc.system.dao.ISysUserDao;
import com.tensai.core.mvc.system.entry.SysUser;
import com.tensai.core.mvc.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 用户service
 *
 * @author Tensai
 * 2018/12/21
 */
@Service
public class SysUserService extends BaseIdServiceImpl<SysUser, ISysUserDao> implements ISysUserService {

    @Autowired
    private ISysUserDao sysUserDao;

    @Override
    protected ISysUserDao getDao() {
        return sysUserDao;
    }

    @Override
    public SysUser login(String username, String password) {
        SysUser sysUser = getDao().findByUsernameAndPassword(username, password);
        return sysUser;
    }
}

