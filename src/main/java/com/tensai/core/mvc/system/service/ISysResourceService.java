package com.tensai.core.mvc.system.service;

import com.tensai.core.mvc.base.service.IIdService;
import com.tensai.core.mvc.base.tools.XTree;
import com.tensai.core.mvc.system.dao.ISysResourceDao;
import com.tensai.core.mvc.system.entry.SysResource;

import java.util.List;

/**
 * @author Tensai
 *         2018-03-22 16:15
 */
public interface ISysResourceService extends IIdService<SysResource, ISysResourceDao> {
	
	List<SysResource> findByResourceString(String url);


	List<SysResource> findByType(String type);

	List<XTree> getUserAuth(List<Integer> checkedList);
}
