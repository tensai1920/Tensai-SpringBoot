package com.tensai.core.mvc.system.entry;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tensai.core.mvc.base.entry.IdEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户组
 *
 * @author Tensai
 * 2018/12/20
 */
@Entity
@Table
public class SysGroup extends IdEntity {

    private String name;
    /**
     * json数组表示权限路径
     */
    private String authJson;
    /**
     * 二进制权限码
     */
    private String authBinCode;
    /**
     * json数组表示菜单路径
     */
    private String menuJson;
    /**
     * 二进制菜单码
     */
    private String menuBinCode;
    /**
     * 多对多关系维护
     */
    private List<SysResource> resources = new ArrayList<>();
    /**
     * 用户
     * 一对多关系维护
     */
    private List<SysUser> users = new ArrayList<>();

    @Column
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column
    public String getAuthJson() {
        return authJson;
    }

    public void setAuthJson(String authJson) {
        this.authJson = authJson;
    }

    @Column
    public String getAuthBinCode() {
        return authBinCode;
    }

    public void setAuthBinCode(String authBinCode) {
        this.authBinCode = authBinCode;
    }

    @Column
    public String getMenuJson() {
        return menuJson;
    }

    public void setMenuJson(String menuJson) {
        this.menuJson = menuJson;
    }

    @Column
    public String getMenuBinCode() {
        return menuBinCode;
    }

    public void setMenuBinCode(String menuBinCode) {
        this.menuBinCode = menuBinCode;
    }

    @ManyToMany(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)
    @JoinTable(name="sys_m2m_resource_group")
    public List<SysResource> getResources() {
        return resources;
    }

    public void setResources(List<SysResource> resources) {
        this.resources = resources;
    }

    @OneToMany(mappedBy = "sysGroup")
    @JsonIgnore
    public List<SysUser> getUsers() {
        return users;
    }

    public void setUsers(List<SysUser> users) {
        this.users = users;
    }
}

