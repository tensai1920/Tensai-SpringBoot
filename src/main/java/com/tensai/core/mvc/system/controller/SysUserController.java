package com.tensai.core.mvc.system.controller;

import com.tensai.core.mvc.base.controller.BaseController;
import com.tensai.core.mvc.base.tools.ResultJson;
import com.tensai.core.mvc.system.entry.SysGroup;
import com.tensai.core.mvc.system.entry.SysUser;
import com.tensai.core.mvc.system.service.ISysGroupService;
import com.tensai.core.mvc.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *  用户控制器
 *
 * @author Tensai
 * 2018/12/29
 */
@Controller
@RequestMapping("/system/user/")
public class SysUserController extends BaseController<SysUser, Integer, ISysUserService> {

    private ISysUserService sysUserService;

    @Autowired
    private ISysGroupService sysGroupService;

    /**
     * @deprecated      修改用户组
     * @param id        用户id
     * @param sysGroup  用户组
     * @return          操作结果
     */
    @RequestMapping("/editGroup")
    @ResponseBody
    public ResultJson editGroup(Integer id, Integer sysGroup) {
        SysGroup group = sysGroupService.findOne(sysGroup);
        SysUser user = getService().findOne(id);
        user.setSysGroup(group);
        return getService().save(user);
    }

    /**
     * 修改密码
     * @param id        用户id
     * @param password  密码
     * @return          操作结果
     */
    @RequestMapping("/editPassword")
    @ResponseBody
    public ResultJson editPassword(Integer id, String password) {
        SysUser user = getService().findOne(id);
        user.setPassword(password);
        return getService().save(user);
    }

    @Override
    protected void initEntity() {
        this.entity = new SysUser();
    }
}

