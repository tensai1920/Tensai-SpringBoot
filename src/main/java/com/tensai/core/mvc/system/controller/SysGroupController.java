package com.tensai.core.mvc.system.controller;

import com.tensai.core.mvc.base.controller.BaseController;
import com.tensai.core.mvc.base.tools.ResultJson;
import com.tensai.core.mvc.system.entry.SysGroup;
import com.tensai.core.mvc.system.service.ISysGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 用户组控制器
 *
 * @author Tensai
 * 2018/12/29
 */
@Controller
@RequestMapping("/system/group/")
public class SysGroupController extends BaseController<SysGroup, Integer, ISysGroupService> {

    @Autowired
    private ISysGroupService sysGroupService;

    @RequestMapping("/updateMenu")
    @ResponseBody
    public ResultJson updateGroupMenus(Integer id, String menus) {
        SysGroup sysGroup = getService().findOne(id);
        sysGroup.setMenuJson(menus);
        return getService().save(sysGroup);
    }

    @RequestMapping("/updateAuth")
    @ResponseBody
    public ResultJson updateAuth(Integer id, String auth) {
        SysGroup sysGroup = getService().findOne(id);
        sysGroup.setAuthJson(auth);
        return getService().save(sysGroup);
    }

    @Override
    protected void initEntity() {
        this.entity = new SysGroup();
    }

    @Override
    protected ISysGroupService getService(){
        return sysGroupService;
    }
}

