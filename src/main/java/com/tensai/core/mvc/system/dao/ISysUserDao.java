package com.tensai.core.mvc.system.dao;

import com.tensai.core.mvc.base.dao.IIdDao;
import com.tensai.core.mvc.system.entry.SysUser;

public interface ISysUserDao extends IIdDao<SysUser> {

    SysUser findByUsernameAndPassword(String username, String password);
}
