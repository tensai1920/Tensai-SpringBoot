package com.tensai.core.mvc.system.service.imp;

import com.tensai.core.mvc.base.service.imp.BaseIdServiceImpl;
import com.tensai.core.mvc.system.dao.ISysGroupDao;
import com.tensai.core.mvc.system.entry.SysGroup;
import com.tensai.core.mvc.system.service.ISysGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 用户service
 *
 * @author Tensai
 * 2018/12/21
 */
@Service
public class SysGroupService extends BaseIdServiceImpl<SysGroup, ISysGroupDao> implements ISysGroupService {

    @Autowired
    private ISysGroupDao sysGroupDao;

    @Override
    protected ISysGroupDao getDao() {
        return sysGroupDao;
    }

}

