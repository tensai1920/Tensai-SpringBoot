package com.tensai.core.mvc.system.service;

import com.tensai.core.mvc.base.service.IIdService;
import com.tensai.core.mvc.system.dao.ISysGroupDao;
import com.tensai.core.mvc.system.entry.SysGroup;

/**
 *
 * @author kissy
 */
public interface ISysGroupService extends IIdService<SysGroup, ISysGroupDao> {


}
