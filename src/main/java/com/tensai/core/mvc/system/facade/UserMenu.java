package com.tensai.core.mvc.system.facade;

import com.tensai.core.mvc.system.entry.SysMenu;

import java.util.ArrayList;
import java.util.List;

/**
 * 左侧菜单外观类
 *
 * @author Tensai
 * 2019/1/2
 */
public class UserMenu {

    private String name;
    private String href;
    private String icon;
    private Boolean spread;
    private List<UserMenu> children;

    public UserMenu(SysMenu sysMenu) {
        this.name = sysMenu.getName();
        this.href = sysMenu.getViewUrl();
        this.icon = sysMenu.getIcon();
        this.spread = false;
        List<SysMenu> sysMenuList = sysMenu.getSysMenuList();
        if (sysMenuList != null) {
            List<UserMenu> userMenuList = new ArrayList<>();
            sysMenuList.forEach(item -> {
                UserMenu userMenu = new UserMenu(item);
                userMenuList.add(userMenu);
            });
            this.children = userMenuList;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Boolean getSpread() {
        return spread;
    }

    public void setSpread(Boolean spread) {
        this.spread = spread;
    }

    public List<UserMenu> getChildren() {
        return children;
    }

    public void setChildren(List<UserMenu> children) {
        this.children = children;
    }
}

