package com.tensai.core.mvc.system.service;

import com.tensai.core.mvc.base.service.IIdService;
import com.tensai.core.mvc.system.dao.ISysUserDao;
import com.tensai.core.mvc.system.entry.SysUser;

public interface ISysUserService extends IIdService<SysUser, ISysUserDao> {

    /**
     * 验证用户登录
     * @param username  用户名
     * @param password  密码
     * @return          用户信息
     */
    SysUser login(String username, String password);
}
