package com.tensai.core.mvc.system.entry;

import com.tensai.core.mvc.base.entry.IdEntity;

import javax.persistence.*;

/**
 * 系统用户
 *
 * @author Tensai on 2018/12/14
 */
@Entity
@Table(name = "sys_user")
public class SysUser extends IdEntity {

    private String username;
    private String password;
    private String nickName;
    private SysGroup sysGroup;

    @Column
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column
    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @JoinColumn
    @ManyToOne
    public SysGroup getSysGroup() {
        return sysGroup;
    }

    public void setSysGroup(SysGroup sysGroup) {
        this.sysGroup = sysGroup;
    }
}

