package com.tensai.core.mvc.system.entry;

import com.tensai.core.mvc.base.entry.IdEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 系统资源路径
 *
 * @author Tensai
 * 2018-03-22 15:45
 */
@Entity
@Table(name = "sys_resource")
public class SysResource extends IdEntity {

    private String resourceString;
    private String resourceId;
    private String methodPath;
    /**
     * 路径名称
     */
    private String name;
    private Integer level = 0;
    private String type;
    private SysResource parent;

    private List<SysResource> sysResourceList;

    private List<SysGroup> groups = new ArrayList<>();

    @Column(name = "resource_string")
    public String getResourceString() {
        return resourceString;
    }

    public void setResourceString(String resourceString) {
        this.resourceString = resourceString;
    }

    @Column(name = "resource_id")
    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    @Column(name = "method_path")
    public String getMethodPath() {
        return methodPath;
    }

    public void setMethodPath(String methodPath) {
        this.methodPath = methodPath;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column
    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Column
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JoinColumn
    @ManyToOne
    public SysResource getParent() {
        return parent;
    }

    public void setParent(SysResource parent) {
        this.parent = parent;
    }

    @ManyToMany(mappedBy = "resources", fetch = FetchType.LAZY)
    public List<SysGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<SysGroup> groups) {
        this.groups = groups;
    }

    @Transient
    public List<SysResource> getSysResourceList() {
        return sysResourceList;
    }

    public void setSysResourceList(List<SysResource> sysResourceList) {
        this.sysResourceList = sysResourceList;
    }
}
