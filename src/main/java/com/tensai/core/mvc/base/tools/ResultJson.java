package com.tensai.core.mvc.base.tools;

import com.tensai.core.constant.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * @author kissy
 * 2018-03-10
 */
public class ResultJson {
    /**
     * 请求状态
     */
    private int status;
    /**
     * 错误码 0:响应成功 1001:session超时
     */
    private int code = 0;
    /**
     * 提示信息
     */
    private String msg;
    private Object data = null;

    public ResultJson(ResponseStatus responseStatus) {
        this.status = responseStatus.getStatus();
        this.msg = responseStatus.getMsg();
    }

    public ResultJson(ResponseStatus responseStatus, Object data) {
        this.status = responseStatus.getStatus();
        this.msg = responseStatus.getMsg();
        this.data = data;
    }

    public ResultJson(int code, String msg) {
        this.status = ResponseStatus.EXCEPTION.getStatus();
        this.msg = msg + ResponseStatus.EXCEPTION.getMsg();
        this.code = code;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public void addData(String key, String value) {
        Map<String, String> map = new HashMap<>();
        map.put(key, value);
        this.data = map;
    }
}

