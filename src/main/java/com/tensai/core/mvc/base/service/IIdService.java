package com.tensai.core.mvc.base.service;

import com.tensai.core.mvc.base.dao.IIdDao;
import com.tensai.core.mvc.base.entry.IdEntity;

/**
 * 基础以ID为主键的service接口
 * @param <T>   实体类
 * @param <D>   数据库操作接口
 * @author Tensai
 */
public interface IIdService<T extends IdEntity, D extends IIdDao<T>> extends IBaseService<T,Integer> {
	
}
