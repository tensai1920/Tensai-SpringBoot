package com.tensai.core.mvc.base.tools;


import com.tensai.core.mvc.base.entry.BaseEntity;
import com.tensai.core.constant.ResponseStatus;

import java.util.List;

/**
 * DataGrid模型
 *
 * @author tensai
 */
public class Grid<T extends BaseEntity> extends ResultJson implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 数据个数
     */
    private long count = 0;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public Grid(List<T> data, long count) {
        super(ResponseStatus.QUERY_SUCCESS, data);
        this.count = count;
    }

}
