package com.tensai.core.mvc.base.entry;

import javax.persistence.*;

/**
 * id为主键的实体基础类
 *
 * @author Tensai
 */
@MappedSuperclass
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class IdEntity extends BaseEntity {

    /**
     * 主键id
     */
    private Integer id;

    @Id
    @Column(name = "id", nullable = false, length = 11, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
