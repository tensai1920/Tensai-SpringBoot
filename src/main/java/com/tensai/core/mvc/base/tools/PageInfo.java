package com.tensai.core.mvc.base.tools;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

/**
 * 分页信息
 *
 * @author kissy
 */
public class PageInfo {

    /**
     * 当前页码
     */
    private int page = 1;
    /**
     * 每页元素个数
     */
    private int rows = 10;
    /**
     * 排序字段
     */
    private String sort;
    /**
     * 排序方式
     */
    private String order = "desc";

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public Pageable toPageable() {
        Pageable p;
        if (StringUtils.isNoneEmpty(sort)) {
            Sort s = new Sort(new Sort.Order("asc".equals(order) ? Direction.ASC : Direction.DESC, sort));
            p = new PageRequest(page > 0 ? page - 1 : 0, rows, s);
        } else {
            p = new PageRequest(page > 0 ? page - 1 : 0, rows);
        }
        return p;
    }

}
