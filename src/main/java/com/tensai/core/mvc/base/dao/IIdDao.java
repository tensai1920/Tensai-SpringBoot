package com.tensai.core.mvc.base.dao;

import com.tensai.core.mvc.base.entry.IdEntity;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * Id为主键的数据库操作类，继承自{@link IBaseDao}
 *
 * @param <T> 实体类继承自{@link IdEntity}
 * @author Tensai
 */
@NoRepositoryBean
public interface IIdDao<T extends IdEntity> extends IBaseDao<T, Integer> {

    /**
     * 根据主键列表查询数据
     *
     * @param ids 主键列表
     * @return 结果集
     */
    List<T> findByIdIn(List<Integer> ids);

    /**
     * 根据主键列表删除数据
     *
     * @param ids 主键列表
     */
    void deleteByIdIn(List<Integer> ids);

}
