package com.tensai.core.mvc.base.service;

import com.tensai.core.mvc.base.dao.IUuidDao;
import com.tensai.core.mvc.base.entry.UuidEntity;

/**
 * 基础以UUID为主键的service接口
 * @param <T>   实体类
 * @param <D>   数据库操作接口
 * @author Tensai
 */
public interface IUuidService<T extends UuidEntity, D extends IUuidDao<T>> extends IBaseService<T, String> {

}
