package com.tensai.core.mvc.base.tools;

import java.io.Serializable;

/**
 * sessionInfo模型
 *
 * @author kissy
 */
public class SessionInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 用户ID
	 */
	private int userId;
	/**
	 * 用户登录名
	 */
	private String loginName;
	/**
	 * 角色名
	 */
	private String roleName;
	/**
	 * 用户区域ID
	 */
	private String userAreaId;
	/**
	 * 用户区域名称
	 */
	private String userAreaName;

	public SessionInfo() {
	}

	public SessionInfo(int userId, String loginName) {
		this.userId = userId;
		this.loginName = loginName;
	}
	
	public int getUserId() {
		return userId;
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public String getLoginName() {
		return loginName;
	}
	
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	
	public String getRoleName() {
		return roleName;
	}
	
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	public String getUserAreaId() {
		return userAreaId;
	}
	
	public void setUserAreaId(String userAreaId) {
		this.userAreaId = userAreaId;
	}
	
	public String getUserAreaName() {
		return userAreaName;
	}
	
	public void setUserAreaName(String userAreaName) {
		this.userAreaName = userAreaName;
	}
	
	@Override
	public String toString() {
		return loginName;
	}
	
}
