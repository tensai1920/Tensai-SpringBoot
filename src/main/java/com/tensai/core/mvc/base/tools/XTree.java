package com.tensai.core.mvc.base.tools;

import com.tensai.core.mvc.system.entry.SysMenu;
import com.tensai.core.mvc.system.entry.SysResource;

import java.util.ArrayList;
import java.util.List;

/**
 * LayUI的X-Tree
 *
 * @author Tensai
 * 2019/1/8
 */
public class XTree {

    private String title;
    private String value;
    private Boolean checked = false;
    private Boolean disabled = false;
    private List<XTree> data;

    public XTree(SysMenu sysMenu, List<Integer> checkList) {
        this(sysMenu, checkList, null);
    }

    public XTree(SysResource sysResource, List<Integer> checkList) {
        this(sysResource, checkList, null);
    }

    private XTree(SysMenu sysMenu, List<Integer> checkList, List<Integer> disableList) {
        this.title = sysMenu.getName();
        this.value = sysMenu.getId().toString();
        if (checkList != null && checkList.contains(sysMenu.getId())) {
            this.checked = true;
        }
        if (disableList != null && disableList.contains(sysMenu.getId())) {
            this.disabled = true;
        }
        List<SysMenu> sysMenuList = sysMenu.getSysMenuList();
        if (sysMenuList != null) {
            List<XTree> xTreeList = new ArrayList<>();
            sysMenuList.forEach(item -> {
                XTree rightMenus = new XTree(item, checkList, disableList);
                xTreeList.add(rightMenus);
            });
            this.data = xTreeList;
        }
    }

    private XTree(SysResource sysResource, List<Integer> checkList, List<Integer> disableList) {
        this.title = sysResource.getName();
        this.value = sysResource.getId().toString();
        if (checkList != null && checkList.contains(sysResource.getId())) {
            this.checked = true;
        }
        if (disableList != null && disableList.contains(sysResource.getId())) {
            this.disabled = true;
        }
        List<SysResource> sysMenuList = sysResource.getSysResourceList();
        if (sysMenuList != null) {
            List<XTree> xTreeList = new ArrayList<>();
            sysMenuList.forEach(item -> {
                XTree rightMenus = new XTree(item, checkList, disableList);
                xTreeList.add(rightMenus);
            });
            this.data = xTreeList;
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public List<XTree> getData() {
        return data;
    }

    public void setData(List<XTree> data) {
        this.data = data;
    }
}

