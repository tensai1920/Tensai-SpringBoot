package com.tensai.core.mvc.base.dao;

import com.tensai.core.mvc.base.entry.UuidEntity;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * Id为主键的数据库操作类，继承自{@link IBaseDao}
 *
 * @param <T> 实体类继承自{@link UuidEntity}
 * @author Tensai
 */
@NoRepositoryBean
public interface IUuidDao<T extends UuidEntity> extends IBaseDao<T, String> {

    /**
     * 根据主键列表查询数据
     *
     * @param uuids 主键列表
     * @return 结果集
     */
    List<T> findByUuidIn(List<String> uuids);

    /**
     * 根据逐渐列表删除数据
     *
     * @param uuids 主键列表
     */
    void deleteByUuidIn(List<String> uuids);

}
