package com.tensai.core.mvc.base.entry;

import javax.persistence.*;

/**
 * UUID为主键的实体基础类
 *
 * @author Tensai
 */
@MappedSuperclass
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class UuidEntity extends BaseEntity {
    /**
     * 主键UUID
     */
    private String uuid;

    @Id
    @Column(name = "UUID", nullable = false, length = 36, updatable = false)
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
