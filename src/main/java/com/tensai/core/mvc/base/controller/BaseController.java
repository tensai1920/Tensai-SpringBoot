package com.tensai.core.mvc.base.controller;

import com.tensai.core.mvc.base.entry.BaseEntity;
import com.tensai.core.mvc.base.service.IBaseService;
import com.tensai.core.mvc.base.tools.Grid;
import com.tensai.core.mvc.base.tools.HqlFilter;
import com.tensai.core.mvc.base.tools.PageInfo;
import com.tensai.core.mvc.base.tools.ResultJson;
import com.tensai.core.constant.ResponseStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 基础Controller类，所有数据库对应的实体类相应的Controller都要继承这个类，以便于获取其中的方法
 *
 * @param <T> 实体
 * @param <P> 实体主键类型
 * @param <I> service接口类
 * @author Tensai
 */
@RestController
public abstract class BaseController<T extends BaseEntity, P, I extends IBaseService<T, P>> {

    /**
     * 实体类定义，protected是为了让子类能够this访问
     */
    protected T entity;

    /**
     * 自动注入service
     */
    @SuppressWarnings("all")
    @Autowired
    protected I service;

    /**
     * 获取service
     *
     * @return service
     */
    protected I getService() {
        return service;
    }

    /**
     * 初始化实体对象
     */
    protected abstract void initEntity();

    /**
     * 根据主键查找实体
     *
     * @param key 主键
     * @return 实体的包装类
     */
    @RequestMapping("/getByKey")
    @ResponseBody
    public ResultJson getById(P key) {
        T t = getService().findOne(key);
        return new ResultJson(ResponseStatus.QUERY_SUCCESS, t);
    }

    /**
     * 根据主键删除数据
     *
     * @param key 主键
     * @return 删除结果
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ResultJson delete(P key) {
        return getService().delete(key);
    }

    /**
     * 批量删除
     *
     * @param keys 主键集
     * @return 操作结果
     */
    @RequestMapping("/deletes")
    @ResponseBody
    public ResultJson deletes(String keys) {
        return getService().deletes(keys);
    }

    /**
     * 保存实体
     *
     * @param entity 实体
     * @return 操作结果
     */
    @RequestMapping("/save")
    @ResponseBody
    public ResultJson save(T entity) {
        return getService().save(entity);
    }

    /**
     * 查找所有实体
     *
     * @return 所有实体
     */
    @RequestMapping("/findAll")
    @ResponseBody
    public ResultJson findAll() {
        return new ResultJson(ResponseStatus.QUERY_SUCCESS, getService().findAll());
    }

    /**
     * 获取分页信息
     *
     * @param g       分页信息
     * @param request 请求
     * @return 结果集
     */
    @RequestMapping("/grid")
    @ResponseBody
    public ResultJson grid(PageInfo g, HttpServletRequest request) {
        HqlFilter<T> filter = new HqlFilter<>(request);
        Page<T> page = getService().page(g, filter);
        return new Grid<>(page.getContent(), page.getTotalElements());
    }

}