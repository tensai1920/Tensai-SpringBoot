package com.tensai.core.mvc.base.service;

import com.tensai.core.mvc.base.entry.BaseEntity;
import com.tensai.core.mvc.base.tools.HqlFilter;
import com.tensai.core.mvc.base.tools.PageInfo;
import com.tensai.core.mvc.base.tools.ResultJson;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * 基础service类，用来控制实体类DAO的操作逻辑
 *
 * @param <T>      实体类
 * @param <IDType> 实体类主键类型
 * @author Tensai
 */
public interface IBaseService<T extends BaseEntity, IDType> {

    /**
     * 保存实体到数据库
     *
     * @param entity 实体
     * @return 操作结果
     */
    ResultJson save(T entity);

    /**
     * 分页信息
     *
     * @param page   分页
     * @param filter 过滤规则
     * @return 结果集
     */
    Page<T> page(PageInfo page, HqlFilter<T> filter);

    /**
     * 根据主键查找
     *
     * @param id 主键
     * @return 结果
     */
    T findOne(IDType id);

    /**
     * 根据主键删除实体
     *
     * @param id 主键
     * @return 操作状态
     */
    ResultJson delete(IDType id);

    /**
     * 删除实体
     *
     * @param t 实体
     * @return 操作状态
     */
    ResultJson delete(T t);

    /**
     * 删除多个实体
     *
     * @param ids 多实体id
     * @return 操作状态
     */
    ResultJson deletes(String ids);

    /**
     * 查询所有实体
     *
     * @return 结果集
     */
    List<T> findAll();
}
