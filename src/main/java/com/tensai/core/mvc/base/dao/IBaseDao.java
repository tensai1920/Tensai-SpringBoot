package com.tensai.core.mvc.base.dao;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.io.Serializable;
import java.util.List;

/**
 * 基础数据库操接口，实现JPA（JPA具体的实现可以自定义配置Factory类。<br>
 * 此接口继承SpingBoot的data的Jpa接口。<br>
 * {@link JpaSpecificationExecutor,PagingAndSortingRepository}
 *
 * @author Tensai
 */
@NoRepositoryBean
public interface IBaseDao<T, IDType extends Serializable> extends JpaSpecificationExecutor<T>, PagingAndSortingRepository<T, IDType> {

    /**
     * 查询全表
     *
     * @return 全表数据
     */
    @Override
    List<T> findAll();

    /**
     * 排序查询
     *
     * @param s 排序规则
     * @return 全表数据
     */
    @Override
    List<T> findAll(Sort s);

}
