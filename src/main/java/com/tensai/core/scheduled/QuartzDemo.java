package com.tensai.core.scheduled;

import java.util.Date;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Quartz设置项目全局的定时任务<br>
 * {@link @Component}注解的意义 泛指组件，当组件不好归类的时候，我们可以使用这个注解进行标注。一般公共的方法我会用上这个注解
 *
 * @author Tensai
 * 2018-03-15 11:24
 */
@Slf4j
@Component
public class QuartzDemo {

    @Scheduled(cron = "3 * * * * ?")
    public void work() {
        log.trace("执行调度任务：" + new Date());
    }

    @Scheduled(fixedRate = 10000)
    public void play() {
        log.warn("执行Quartz定时器任务：" + new Date());
    }

    @Scheduled(cron = "0 * * * * ?")
    public void doSomething() {
        log.error("每2秒执行一个的定时任务：" + new Date());
    }

    @Scheduled(cron = "1 * * * * ? ")
    public void goWork() {
        log.info("每一小时执行一次的定时任务：" + new Date());
    }

    @Scheduled(cron = "2 * * * * ? ")
    public void debug() {
        log.debug("每一小时执行一次的定时任务：每一小时执行一次的定时任务：每一小时执行一次的定时任务：每一小时执行一次的定时任务：每一小时执行一次的定时任务：每一小时执行一次的定时任务：每一小时执行一次的定时任务：每一小时执行一次的定时任务：" + new Date());
    }
}